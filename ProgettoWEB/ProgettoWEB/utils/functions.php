<?php
function registerLoggedUser($user){
    $_SESSION["ID_Utente"] = $user["ID_Utente"];
    $_SESSION["nome"] = $user["Nome"];
    $_SESSION["cognome"] = $user["Cognome"];
    $_SESSION["venditore"] = $user["Venditore"];
}

function isUserLoggedIn(){
    return !empty($_SESSION["ID_Utente"]);
}

function isVenditoreLoggedIn(){
    if (!empty($_SESSION["venditore"])){
        return true;
    }
    return false;
}


?>