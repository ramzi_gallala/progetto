<div class="py-2 text-center">
    <h3 class="text-center font-weight-bold">Lista articoli:</h3>
        <?php 
            $idLbl = 0;
            foreach($templateParams["articoli"] as $articolo): 
            if (!empty($articolo["quantita"]) ): 
        ?>
            <div class="article">
                <div class="flipper">
                    <div class="front">
                        <div class="article-image">
                            <img src="./images/articles/<?php echo $articolo["immagine"] ?>" alt="<?php echo $articolo["immagine"]?>" />
                        </div>
                        <div class="article-details">
                            <div class="article-price">
                                <p><?php echo $articolo["prezzo"]; ?></p>
                            </div>
                            <div class="article-title">
                                <h4><?php echo $articolo["titolo"]; ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="back">
                        <h4><?php echo $articolo["titolo"]; ?></h4>
                        <p><strong><?php echo $articolo["descrizione"]; ?></strong></p>
                        <p><strong>Prezzo: <?php echo $articolo["prezzo"]; ?></strong></p>
                        <p><strong>Spedizione entro 3gg</strong></p>
                    </div>
                </div>
                <div class="article-details py-1">
                    <form action="gestione-articolo.php" method="GET">
                        <?php if(isset($_SESSION["ID_Utente"])): ?>
                        <label class="invisible" for="acqst<?php echo $idLbl ?>">s</label><input id="acqst<?php echo $idLbl++ ?>" style="display:none;" name="action" value="3"/>
                        <label class="invisible" for="acqst<?php echo $idLbl ?>">s</label><input id="acqst<?php echo $idLbl++ ?>" style="display:none;" name="id" value="<?php echo $articolo["ID_Articolo"]; ?>"/>
                        <label class="invisible" for="acqst<?php echo $idLbl ?>">s</label><input id="acqst<?php echo $idLbl++ ?>" type="submit" name="acquista" value="ACQUISTA" class="btn-primary btn-sm font-weight-bold" >
                        <?php else: ?>
                        <label class="invisible" for="acqst<?php echo $idLbl ?>">s</label><input id="acqst<?php echo $idLbl++ ?>" type="submit" value="ACQUISTA" class="btn-primary btn-sm font-weight-bold" disabled>
                        <?php endif; ?>
                    </form>
                </div>
            </div>
        <?php endif; endforeach;?>
</div>