<div class="row">
                <div class="col-12">
					<?php foreach($templateParams["previews"] as $preview): ?>
                    <article class="bg-white border mt-4 mb-4">
                        <header class="px-2">
                            <h2>Id Ordine: <?php 
                              if($_SESSION["venditore"]==0){
                                echo $preview["ID_ordini_g"];
                              }
                              else{
                                echo $preview["ID_Articolo"];
                              } 
                            ?>
                            </h2>
                        </header>
                        <div class="px-4">
                            <p><?php 
                              if($_SESSION["venditore"]==0){
                                echo 'Prezzo totale: '. $preview["Somma"];
                              }
                              else{
                                echo 'Nome prodotto: '.$preview["titolo"];
                              } 
                            ?></p>
                            <p><?php
                              if($_SESSION["venditore"]==0){
                                echo 'Stato: Partito dal magazzino';
                              }
                              else{
                                echo 'Finita quantità';
                              }
                            ?></p>
                        </div>
                        <?php 
                              if($_SESSION["venditore"]==0){
                                echo '<footer class="text-right pb-3 px-3">
                                        <a class="btn btn-light" href="Orderdetail.php?ID_ordine_g='.$preview["ID_ordini_g"].'&Somma='.$preview["Somma"].'">Visualizza dettagli</a>
                                      </footer>';
                              } 
                        ?>
                        
                    </article>
					<?php endforeach; ?>
                  
                </div>
</div>
<div class="pushNotifiche"> </div>
