<h3 class="text-center font-weight-bold">Prodotti in vendita:</h3>
    <?php foreach($templateParams["articoli"] as $articolo): ?>
        <div class="pezzo">
            <div class="articles">
                <div class="image">
                    <img src="./images/articles/<?php echo $articolo["immagine"] ?>" alt="" />
                </div>
                <div class="description">
                    <h4><?php echo $articolo["titolo"]; ?></h4>
                    <p><strong><?php echo $articolo["descrizione"]; ?></strong></p>
                    <p><strong>Prezzo: €<?php echo $articolo["prezzo"]; ?></strong></p>
                    <p><strong>Spedizione entro 3gg</strong></p>
                    
                </div>
            </div>
            <div class="altro">
                <div class="quantita">
                    <form action="#"  method="POST" style="display: inline-flex;">
                        <div class="label">
                            <label for="qnt">Quantità:
                                <input id="qnt" type="number" name="<?php echo $articolo["ID_Articolo"] ?>" min="0" max="130" step="1" value="<?php echo $articolo["quantita"]; ?>" class="text-center"  >
                            </label>
                        </div>
                        <div class="bottone">
                            <input type="submit" value="CONFERMA" class="btn-primary btn-sm font-weight-bold">
                        </div>
                    </form>
                </div>
                <div class="delete">                   
                    <a href="gestione-articolo.php?action=1&id=<?php echo $articolo["ID_Articolo"]; ?>" ><img src="./images/others/deleteArticleImg.png" alt="delete article"/></a>
                </div>
            </div>
        </div>
    <?php endforeach;?>
    <hr>
    <hr>
    <h2 class="text-center font-weight-bold">Aggiungi articolo:</h2>
    <div class="row text-center">
        <div class="col-12 col-md-6 pl-1">
            <form class="mt-4 mb-4 pl-2 pr-4 py-4" method="POST" action="venditore.php">
                    <div class="form-group row text-center">
                        <label for="titolo" class="col-4">Titolo</label>
                        <input type="text" class="form-control col-8" id="titolo" name="titolo" placeholder="Inserisci nome">
                    </div>
                    <div class="form-group row text-center">
                        <label for="immagine" class="col-4">Immagine</label>
                        <input type="file" class="col-8" id="immagine" name="immagine">
                    </div>
                    <div class="form-group row text-center">
                        <label for="prezzo" class="col-4">Prezzo</label>
                        <input type="number" step="0.01" class="form-control col-8" id="prezzo" name="prezzo" placeholder="Inserisci prezzo">
                    </div>
                    <div class="form-group row text-center">
                        <label for="quantita" class="col-4">Quantita</label>
                        <input type="number" class="form-control col-8" id="quantita" name="quantita" placeholder="Inserisci quantita">
                    </div>
                    <div class="form-group row text-center">
                        <label for="descrizione" class="col-4">Descrizione</label>
                        <textarea cols="40" rows="5" class="form-control col-8" id="descrizione" name="descrizione" placeholder="Inserisci descrizione"></textarea>
                    </div>
                    <div class="form-group text-center">
                        <button type="submit" name="submit" class="btn btn-secondary border border-dark">Invia</button>
                    </div>
            </form>
        </div>
    </div>   