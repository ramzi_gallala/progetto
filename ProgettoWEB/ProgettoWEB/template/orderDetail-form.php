<div class="row">
                <div class="col-12 px-4">
                    <h2 class="text-center py-3">Id Ordine: <?php echo $_GET["ID_ordine_g"]?></h2>
                    <div class="col-12">
                        <p>Prezzo totale: <?php echo $_GET["Somma"] ?></p>
                        <p>Stato: Partito dal magazzino</p>
                    </div>
                    <?php foreach($templateParams["articoli"] as $articolo): ?>
                      <article class="bg-light border mt-4 mb-4">
                            <div class="row">
                                <div class="col-6 px-5">
                                    <header>
                                        <h3><?php echo $articolo["titolo"]?></h3>
                                        <img class="img-fluid py-3" src="./images/articles/<?php echo $articolo["immagine"]?>" alt="<?php echo $articolo["titolo"] ?>" />
                                    </header>
                                </div>
                                <div class="col-6 py-5">
                                    <div>
                                        <p>Prezzo: <?php echo $articolo["prezzo"]?></p>
                                        <p>Quantità: <?php echo $articolo["Quantità"]?></p>
                                    </div>
                                </div>
                            </div>
                        </article>
                  <?php endforeach; ?>
                </div>
</div>