<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="../style/style.css" >
    <title>ELECTRONICS</title>
</head>
<body>
    <div class="container-fluid">
        <div class="row ">
            <div class="col-12 px-0 bg-info">
                <header>
                    <a href="./index.html">
                        <img class="img-fluid col-10 float-left pr-0 pl-5" src="../images/others/logo.png" id="logo" alt=""/>
                    </a>
                    <img class="img-fluid col-2 float-right pt-5" src="../images/others/LogoEntrato.png" id="user" alt="User" />
                </header>
            </div>
        </div>
        <div class="row border-top  border-bottom border-primary">
            <div class="col-12 px-0">
                <ul class="nav">
                    <li class="nav-item col-4 border-right btn-secondary">
                        <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./notifiche.php">Notifiche</a>
                    </li>
                    <li class="nav-item col-4 border-right btn-secondary">
                        <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./ordini.php">Ordini</a>
                    </li>
                    <li class="nav-item col-4 btn-secondary">
                        <a class="nav-link mt-1 text-center px-0 font-weight-bold text-white" href="./carrello.php">Carrello</a>
                    </li>
                </ul>
            </div>
        </div>
        <main>
            <div class="row">
                <div class="col-12">
                    <article class="bg-white border mt-4 mb-4">
                        <header class="px-2">
                            <h2><a href="" class="fs-6">Id ordine: 1</a></h2>
                        </header>
                        <section class="px-4">
                            <p>Prezzo articolo</p>
                            <p>ordine consegnato</p>
                        </section>
                        <footer class="text-right pb-3 px-3">
                            <a class="btn btn-light" href="./orderDetail.html">Visualizza dettagli</a>
                        </footer>
                    </article>
                    <article class="bg-white border mt-4 mb-4">
                        <header class="px-2">
                            <h2><a href="">Id ordine: 2</a></h2>
                        </header>
                        <section class="px-4">
                            <p>Prezzo articolo</p>
                            <p>ordine spedito</p>
                        </section>
                        <footer class="text-right pb-3 px-3">
                            <a class="btn btn-light" href="./orderDetail.html">Visualizza dettagli</a>
                        </footer>
                    </article>
                </div>
            </div>
            
        </main>
        <div class="row">
            <div class="col-12 px-0">
                <footer class="py-3 text-white bg-dark">
                    <h2 class="text-center col-12">Eletronics S.r.l.</h2>
                    <div class="row">
                        <div class="col-6">
                            <h3 class="text-center">chi siamo</h3>
                            <p class="text-center">
                                Davide Degli Esposti <br/>
                                Davide Cellot<br/>
                                Ramzi
                            </p>
                        </div>
                        <div class="col-6">
                            <h3 class="text-center">dove siamo</h3>
                            <p class="text-center">
                                Cesena (FC) 47521 <br/>
                                Via Niccolò Macchiavelli
                            </p>
                        </div>
                    </div>   
                </footer>
            </div>
        </div>
    </div>
</body>
</html>