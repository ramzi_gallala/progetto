-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 10, 2021 alle 23:53
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eletronics_db`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `articoli`
--

CREATE TABLE `articoli` (
  `ID_Articolo` int(11) NOT NULL,
  `titolo` varchar(50) NOT NULL,
  `descrizione` varchar(100) NOT NULL,
  `prezzo` double(10,2) NOT NULL,
  `quantita` int(11) NOT NULL,
  `immagine` varchar(50) DEFAULT NULL,
  `ID_utente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `articoli`
--

INSERT INTO `articoli` (`ID_Articolo`, `titolo`, `descrizione`, `prezzo`, `quantita`, `immagine`, `ID_utente`) VALUES
(1, 'SUPPORTO PC', 'Angolazione regolabile. Porta Notebook Pieghevole. Supporto per MacBook. Air/PRO, Dell, Lenovo', 26.99, 2, 'supportoPC.jpg', 2),
(2, 'TASTIERA E MOUSE', 'TedGem. Senza fili, USB. Ergonomico Mouse e Tastiera. USB nano per PC', 33.99, 8, 'tast_mouse.jpg', 1),
(3, 'HUB USB C', 'Adattatore Multiporta 7 in 1. Ingresso HDMI 4k. 3 porte USB, Lettore SD. Grigio spaziale', 23.99, 7, 'hubUSB.jpg', 1),
(4, 'Cavo USB Type-C', 'Rampow. Carica Rapida. Trasmissione compatibile con: Samsung S9/Note 8/S8. Huawei P10/P9', 6.99, 9, 'cavoUSB.jpg', 2),
(5, 'ADATTATORE CAR', 'Adattatore caricatore del portatile. Compatibile con: HP G2 G3 G4 G5. HP ProBook G3 G4 G5 G6. Connet', 19.56, 0, 'caricabatteria.jpg', 1),
(7, 'paapaa', 'asxxczzxc', 25.40, 2, 'tast_mouse.jpg', 14);

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE `carrello` (
  `ID_Carrello` int(11) NOT NULL,
  `ID_Articolo` int(11) NOT NULL,
  `ID_Utente` int(11) NOT NULL,
  `Quantità` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`ID_Carrello`, `ID_Articolo`, `ID_Utente`, `Quantità`) VALUES
(13, 1, 1, 1),
(14, 4, 1, 4),
(37, 3, 15, 2),
(38, 2, 15, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE `ordini` (
  `ID_Ordini` int(11) NOT NULL,
  `ID_Articolo` int(11) NOT NULL,
  `ID_Utente` int(11) NOT NULL,
  `Quantità` int(11) NOT NULL,
  `Stato` varchar(50) NOT NULL,
  `Data` datetime NOT NULL,
  `ID_ordini_g` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`ID_Ordini`, `ID_Articolo`, `ID_Utente`, `Quantità`, `Stato`, `Data`, `ID_ordini_g`) VALUES
(1, 1, 3, 3, 'Conseganto', '2021-02-07 08:00:00', 1),
(2, 2, 3, 3, 'Conseganto', '2021-02-07 08:00:00', 1),
(3, 2, 3, 3, 'Conseganto', '2021-02-07 10:00:00', 3),
(4, 2, 1, 2, 'Conseganto', '2021-02-07 18:30:00', 4),
(7, 1, 1, 1, 'Conseganto', '2021-02-09 00:45:49', 5),
(8, 4, 1, 4, 'Conseganto', '2021-02-09 00:45:49', 5),
(9, 2, 1, 1, 'Conseganto', '2021-02-09 00:49:58', 6),
(10, 5, 1, 2, 'Conseganto', '2021-02-09 00:49:58', 6),
(11, 1, 1, 2, 'Conseganto', '2021-02-09 00:49:58', 6),
(12, 3, 1, 1, 'Conseganto', '2021-02-09 00:52:14', 7),
(13, 4, 12, 3, 'Conseganto', '2021-02-09 19:58:06', 8),
(14, 5, 12, 1, 'Conseganto', '2021-02-09 19:58:06', 8),
(15, 4, 12, 1, 'Conseganto', '2021-02-09 20:27:27', 9),
(16, 3, 12, 1, 'Conseganto', '2021-02-09 20:27:27', 9),
(17, 4, 15, 2, 'Conseganto', '2021-02-09 23:43:13', 10),
(18, 4, 15, 1, 'Conseganto', '2021-02-10 17:13:21', 11),
(19, 4, 15, 1, 'Conseganto', '2021-02-10 17:21:10', 12),
(20, 5, 15, 1, 'Conseganto', '2021-02-10 17:21:10', 12);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini_g`
--

CREATE TABLE `ordini_g` (
  `ID_ordini_g` int(11) NOT NULL,
  `Somma` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ordini_g`
--

INSERT INTO `ordini_g` (`ID_ordini_g`, `Somma`) VALUES
(1, 50),
(3, 20),
(4, 80),
(5, 54),
(6, 127),
(7, 23.99),
(8, 40.53),
(9, 30.98),
(10, 13.98),
(11, 6.99),
(12, 26.55);

-- --------------------------------------------------------

--
-- Struttura della tabella `utenti`
--

CREATE TABLE `utenti` (
  `ID_Utente` int(11) NOT NULL,
  `Nome` varchar(50) NOT NULL,
  `Cognome` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Venditore` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `utenti`
--

INSERT INTO `utenti` (`ID_Utente`, `Nome`, `Cognome`, `Email`, `Password`, `Venditore`) VALUES
(1, 'Giovanna', 'Pollini', 'giovanna.pollini@unibo.it', 'gio.pollini5', 0),
(2, 'Fabio', 'Rossi', 'fabio.rossi@unibo.it', 'fabioRossi123', 1),
(3, 'Ramzi', 'Gallala', 'a@a.it', '1', 0),
(7, 'eee', 'ccd', 'hakani1442@hubopss.com', 'N', 0),
(8, 'eee', 'ccd', 'hakani1442@hubopss.com', 'N', 0),
(9, 'eee', 'ccd', 'hakani1442@hubopss.com', 'b', 0),
(10, 'eee', 'ccd', 'hakani1442@hubopss.com', 'n', 1),
(11, 'eee', 'ccd', 'hakani1442@hubopss.com', 'b', 1),
(12, 'eee', 'ccd', 'tutto.maiuscolo@hotmail.it', 'n', 0),
(13, 'eee', 'ccd', 'hakani1442@hubopss.com', 'n', 1),
(14, 'degli esposti', 'davide', 'dadada.99@gmail.com', '123', 1),
(15, 'qwqw', 'wewe', 'wewe@gmail.com', '123', 0);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `articoli`
--
ALTER TABLE `articoli`
  ADD PRIMARY KEY (`ID_Articolo`),
  ADD KEY `ID_utente` (`ID_utente`);

--
-- Indici per le tabelle `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`ID_Carrello`),
  ADD KEY `ID_Articolo` (`ID_Articolo`),
  ADD KEY `ID_Utente` (`ID_Utente`);

--
-- Indici per le tabelle `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`ID_Ordini`),
  ADD KEY `ID_Articolo` (`ID_Articolo`),
  ADD KEY `ID_Utente` (`ID_Utente`),
  ADD KEY `ID_ordini_g` (`ID_ordini_g`);

--
-- Indici per le tabelle `ordini_g`
--
ALTER TABLE `ordini_g`
  ADD PRIMARY KEY (`ID_ordini_g`);

--
-- Indici per le tabelle `utenti`
--
ALTER TABLE `utenti`
  ADD PRIMARY KEY (`ID_Utente`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `articoli`
--
ALTER TABLE `articoli`
  MODIFY `ID_Articolo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT per la tabella `carrello`
--
ALTER TABLE `carrello`
  MODIFY `ID_Carrello` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT per la tabella `ordini`
--
ALTER TABLE `ordini`
  MODIFY `ID_Ordini` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT per la tabella `ordini_g`
--
ALTER TABLE `ordini_g`
  MODIFY `ID_ordini_g` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `utenti`
--
ALTER TABLE `utenti`
  MODIFY `ID_Utente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `articoli`
--
ALTER TABLE `articoli`
  ADD CONSTRAINT `articoli_ibfk_1` FOREIGN KEY (`ID_utente`) REFERENCES `utenti` (`ID_Utente`);

--
-- Limiti per la tabella `carrello`
--
ALTER TABLE `carrello`
  ADD CONSTRAINT `carrello_ibfk_1` FOREIGN KEY (`ID_Utente`) REFERENCES `utenti` (`ID_Utente`),
  ADD CONSTRAINT `carrello_ibfk_2` FOREIGN KEY (`ID_Articolo`) REFERENCES `articoli` (`ID_Articolo`);

--
-- Limiti per la tabella `ordini`
--
ALTER TABLE `ordini`
  ADD CONSTRAINT `ordini_ibfk_1` FOREIGN KEY (`ID_Utente`) REFERENCES `utenti` (`ID_Utente`),
  ADD CONSTRAINT `ordini_ibfk_2` FOREIGN KEY (`ID_ordini_g`) REFERENCES `ordini_g` (`ID_ordini_g`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `ordini_ibfk_3` FOREIGN KEY (`ID_Articolo`) REFERENCES `articoli` (`ID_Articolo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
